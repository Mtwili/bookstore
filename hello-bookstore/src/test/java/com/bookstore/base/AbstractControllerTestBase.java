package com.bookstore.base;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
public abstract class AbstractControllerTestBase {
	/**
	 * The REST documentation configuration
	 */
	@Rule
	public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("apidoc/snippets");

	/**
	 * The web application context (injected by spring via the
	 * WebAppConfiguration annotation)
	 */
	@Resource
	protected WebApplicationContext webApplicationContext;

	/**
	 * The document object
	 */
	protected RestDocumentationResultHandler documentationHandler;

	/**
	 * The mock mvc object
	 */
	protected MockMvc mockMvc;

	@Before
	public void setUp() {
		this.documentationHandler = document("{method-name}", //
				preprocessRequest(removeHeaders("Authorization")), //
				preprocessResponse(prettyPrint()));

		this.mockMvc = MockMvcBuilders//
				.webAppContextSetup(this.webApplicationContext)//
				.apply(documentationConfiguration(this.restDocumentation))//
				.alwaysDo(documentationHandler)//
				.build();
	}

	/**
	 * Links snippet generator that provides a base description for self links
	 * and ignores the curies link
	 *
	 * public static LinksSnippet links(LinkDescriptor... descriptors) { return
	 * HypermediaDocumentation .links(halLinks(),
	 * linkWithRel("self").description("Link to this resource.").optional(),
	 * linkWithRel("curies").optional().ignored()) .and(descriptors); }
	 */
}
