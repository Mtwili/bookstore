package com.bookstore.json;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.rest.application.MainBookstore;

@RunWith(SpringRunner.class)
@JsonTest
@ContextConfiguration(classes = { MainBookstore.class })
public class CreateBookDtoJsonTests {

	@Autowired
	private JacksonTester<CreateBookDto> json;

	@Test
	public void serializeJson() throws IOException {
		CreateBookDto details = new CreateBookDto();
		details.setBarcode("4003994155486");
		details.setTitle("Gamep");
		details.setNumberOfPages(1L);
		details.setPrice(new BigDecimal(1.0));
		details.setPublishers(Arrays.asList(1L));
		details.setQuantity(1151L);
		details.setWriters(Arrays.asList(3L, 2L));

		assertThat(this.json.write(details)).isEqualToJson("/com/bookstore/domain/dto/crud/createBookDtoFull.json");
	}

	@Test
	public void deserializeJson() throws IOException {
		CreateBookDto details = new CreateBookDto();
		details.setBarcode("4003994155486");
		details.setTitle("Gamep");
		details.setNumberOfPages(1L);
		details.setPrice(new BigDecimal(1.0));
		details.setPublishers(Arrays.asList(1L));
		details.setQuantity(1151L);
		details.setWriters(Arrays.asList(3L, 2L));

		assertThat(this.json.read("/com/bookstore/domain/dto/crud/createBookDtoFull.json"))
				.isEqualToComparingFieldByField(details);
	}
}
