package com.bookstore.mvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bookstore.base.AbstractControllerTestBase;
import com.bookstore.domain.components.BookConverter;
import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.dto.detail.BookDetailDto;
import com.bookstore.domain.dto.detail.PublisherDetailDto;
import com.bookstore.domain.dto.detail.WriterDetailDto;
import com.bookstore.domain.services.BookService;
import com.bookstore.rest.application.MainBookstore;
import com.bookstore.rest.controllers.BookController;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidatorContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
@ContextConfiguration(classes = { MainBookstore.class })
@AutoConfigureRestDocs("apidoc/snippets")
@ComponentScan(basePackageClasses = { BookController.class })
public class BookControllerTests extends AbstractControllerTestBase {

  private static final Logger LOG = LoggerFactory.getLogger(BookControllerTests.class);

  @MockBean
  private BookService bookService;

  @MockBean
  private ConstraintValidatorContext constraintValidatorContext;

  @MockBean
  private BookConverter bookConverter;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void retrieveBookTest() throws Exception {
    LOG.info("Testing get book by id in controller.");
    BookDetailDto details = createBookDetail();

    given(this.bookService.getBookById(1L)).willReturn(details);

    this.mockMvc
        .perform(get("/book/{id}", 1)//
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))//
        .andExpect(status().isOk())//
        .andDo(document("Retrieve book", //
            pathParameters(//
                parameterWithName("id").description("Book's id")), //
            responseFields(//
                fieldWithPath("id").type(JsonFieldType.NUMBER).description("Book's id"), //
                fieldWithPath("barcode").type(JsonFieldType.STRING).description("Book's barcode"), //
                fieldWithPath("title").type(JsonFieldType.STRING).description("Book's title"), //
                fieldWithPath("writers").type(JsonFieldType.ARRAY).description("Book's list of writers"), //
                fieldWithPath("publishers").type(JsonFieldType.ARRAY).description("Book's list of publishers"), //
                fieldWithPath("numberOfPages").type(JsonFieldType.NUMBER).description("Book's number of pages"), //
                fieldWithPath("price").type(JsonFieldType.NUMBER).description("Book's price"), //
                fieldWithPath("quantity").type(JsonFieldType.NUMBER).description("Book's quantity")//
            )

    ));
  }

  // TODO
  @Test
  public void createBookTest() throws Exception {
    LOG.info("Testing create book in controller.");

    CreateBookDto details = createBook();
    String detailsStr = "{  'barcode': 4003994155486,  'title': 'Gamep',  'writers': [3,2],  'publishers': [1],  'numberOfPages': '1',  'price': 1.0, "//
        + " 'quantity': 1151}";
    BookDetailDto bookDetailDto = createBookDetail();
    given(this.bookService.create(details)).willReturn(bookDetailDto);

    this.mockMvc
        .perform(//
            post("/book/create")//
                .content(detailsStr)//
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isCreated())//
        .andDo(document("Create book", //
            responseFields(//
                fieldWithPath("id").description("Id of created book"))));
  }

  // TODO

  public void getBooksShouldReturnMakeAndModel() throws Exception {

    LOG.info("Testing get all available books in controller.");

    List<BookDetailDto> books = createBooks();
    given(this.bookService.getAllBooks()).willReturn(books);
    this.mockMvc
        .perform(get("/book")//
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))//
        .andExpect(status().isOk())//
        .andDo(document("Retrieve all books", //
            pathParameters(//
                parameterWithName("id").description("Book's id")), //
            responseFields(//
                fieldWithPath("id").type(JsonFieldType.NUMBER).description("Book's id"), //
                fieldWithPath("barcode").type(JsonFieldType.STRING).description("Book's barcode"), //
                fieldWithPath("title").type(JsonFieldType.STRING).description("Book's title"), //
                fieldWithPath("writers").type(JsonFieldType.ARRAY).description("Book's list of writers"), //
                fieldWithPath("publishers").type(JsonFieldType.ARRAY).description("Book's list of publishers"), //
                fieldWithPath("numberOfPages").type(JsonFieldType.NUMBER).description("Book's number of pages"), //
                fieldWithPath("price").type(JsonFieldType.NUMBER).description("Book's price"), //
                fieldWithPath("quantity").type(JsonFieldType.NUMBER).description("Book's quantity")//
            )));

  }

  private List<BookDetailDto> createBooks() {
    List<BookDetailDto> books = new ArrayList<>();

    BookDetailDto details = createBookBase(1L, "4003994155486", "Gamep", 1L, 1.0, 1151L);

    List<PublisherDetailDto> publishers = new ArrayList<>();
    publishers.add(createPublisher(1L, "Srbija", "Beograd", "Vulkan"));
    details.setPublishers(publishers);

    List<WriterDetailDto> writers = new ArrayList<>();
    writers.add(createWriter(3L, "Laza", "Kostic"));
    writers.add(createWriter(2L, "Jovan", "Ducic"));
    details.setWriters(writers);

    books.add(details);

    details = createBookBase(2L, "9780201134476", "Knjiga druga", 13L, 121.32, 1151L);

    publishers = new ArrayList<>();
    publishers.add(createPublisher(2L, "Srbija", "Novi Sad", "Laguna"));
    details.setPublishers(publishers);

    writers = new ArrayList<>();
    writers.add(createWriter(3L, "Laza", "Kostic"));
    details.setWriters(writers);

    books.add(details);

    return books;
  }

  private BookDetailDto createBookDetail() {

    BookDetailDto details = createBookBase(1L, "4003994155486", "Gamep", 1L, 1.0, 1151L);

    List<PublisherDetailDto> publishers = new ArrayList<>();
    publishers.add(createPublisher(1L, "Srbija", "Beograd", "Vulkan"));
    details.setPublishers(publishers);

    List<WriterDetailDto> writers = new ArrayList<>();
    writers.add(createWriter(3L, "Laza", "Kostic"));
    writers.add(createWriter(2L, "Jovan", "Ducic"));
    details.setWriters(writers);

    return details;
  }

  private BookDetailDto createBookBase(Long id, String barcode, String title, Long numberOfpages, Double price, Long quantity) {
    BookDetailDto details = new BookDetailDto();

    details.setId(id);
    details.setBarcode(barcode);
    details.setTitle(title);
    details.setNumberOfPages(numberOfpages);
    details.setPrice(new BigDecimal(price));
    details.setQuantity(quantity);

    return details;
  }

  private CreateBookDto createBook() {
    CreateBookDto details = new CreateBookDto();
    details.setBarcode("4003994155486");
    details.setTitle("Gamep");
    details.setNumberOfPages(1L);
    details.setPrice(new BigDecimal(1.0));

    List<Long> publishers = new ArrayList<>();

    publishers.add(1L);
    details.setPublishers(publishers);

    details.setQuantity(1151L);

    List<Long> writers = new ArrayList<>();

    writers.add(3L);
    writers.add(2L);

    details.setWriters(writers);

    return details;

  }

  private PublisherDetailDto createPublisher(Long id, String country, String headquarters, String name) {
    PublisherDetailDto publisher = new PublisherDetailDto();
    publisher.setId(id);
    publisher.setCountry(country);
    publisher.setHeadquarters(headquarters);
    publisher.setName(name);

    return publisher;

  }

  private WriterDetailDto createWriter(Long id, String name, String surname) {
    WriterDetailDto writer = new WriterDetailDto();
    writer.setId(id);
    writer.setName(name);
    writer.setSurname(surname);
    return writer;
  }
}
