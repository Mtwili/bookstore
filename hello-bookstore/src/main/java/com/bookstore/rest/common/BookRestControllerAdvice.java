package com.bookstore.rest.common;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javassist.NotFoundException;

/**
 * Prototype controller advice to handle exceptions
 */
@RestControllerAdvice(basePackages = { "com.bookstore.rest.controllers" })
public class BookRestControllerAdvice {
	private static final Logger log = LoggerFactory.getLogger(BookRestControllerAdvice.class);
	private static final String MESSAGE = "Exception caught, returning ";

	@ExceptionHandler(BindException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorBody validationError(BindException ex) {
		log.error(MESSAGE + "BindException");
		BindingResult result = ex.getBindingResult();
		final List<FieldError> fieldErrors = result.getFieldErrors();
		ErrorBody errors = new ErrorBody(HttpStatus.BAD_REQUEST.value(), "BindException");

		for (FieldError fieldError : fieldErrors) {
			errors.addFieldError(fieldError.getField(), fieldError.getDefaultMessage(), fieldError.getCode());
		}

		return errors;
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorBody handleExceptions(Exception e) {
		log.error(MESSAGE);
		return new ErrorBody(HttpStatus.INTERNAL_SERVER_ERROR.value(), MESSAGE);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(HttpMessageNotReadableException e) {
		log.error(MESSAGE + "HttpMessageNotReadableException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "HttpMessageNotReadableException");
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(HttpRequestMethodNotSupportedException e) {
		log.error(MESSAGE + "HttpRequestMethodNotSupportedException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "HttpRequestMethodNotSupportedException");
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(MethodArgumentNotValidException e) {
		log.error(MESSAGE + "MethodArgumentNotValidException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "MethodArgumentNotValidException");
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(IllegalArgumentException e) {
		log.error(MESSAGE + "IllegalArgumentException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "IllegalArgumentException");
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(NotFoundException e) {
		log.error(MESSAGE + "NotFoundException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "NotFoundException");
	}

	@ExceptionHandler(BookException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorBody handleExceptions(BookException e) {
		log.error(MESSAGE + "BookException");
		return new ErrorBody(HttpStatus.BAD_REQUEST.value(), "BookException");
	}

}
