package com.bookstore.rest.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

public class ErrorBody {

	private final int status;
	private final String message;
	private List<FieldError> fieldErrors = new ArrayList<>();

	public ErrorBody(int i, String message) {
		super();
		this.message = message;
		this.status = i;
	}

	public String getMessage() {
		return message;
	}

	public int getStatus() {
		return status;
	}

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void addFieldError(String path, String message, String argument) {
		FieldError error = new FieldError(path, message, argument);
		fieldErrors.add(error);
	}

}
