package com.bookstore.rest.application;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages = { "com.bookstore" })
@EnableJpaRepositories(basePackages = "com.bookstore.domain.repositories")
@EntityScan(basePackages = { "com.bookstore.domain.entities" })
@EnableTransactionManagement
@EnableWebMvc
public class BaseConfiguration {

}
