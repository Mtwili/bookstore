package com.bookstore.rest.validators;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.repositories.WriterRepository;

@Component
public class CreateBookValidator implements Validator {

	@Autowired
	WriterRepository writerRepository;

	/**
	 * This Validator validates *just* Person instances
	 */
	@Override
	public boolean supports(Class clazz) {
		return CreateBookDto.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		CreateBookDto createBookDto = (CreateBookDto) obj;
		List<Long> writersId = createBookDto.getWriters();

		for (Long id : writersId) {
			if (!writerRepository.exists(id)) {
				errors.rejectValue("writers", "non.existing.writer");
			}
		}
	}
}