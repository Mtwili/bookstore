package com.bookstore.rest.constraints.validators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bookstore.domain.repositories.WriterRepository;
import com.bookstore.rest.constraints.WriterConstraint;

@Component
public class WriterConstraintValidator implements ConstraintValidator<WriterConstraint, List<Long>> {

	public static final String MESSAGE = "error.invalid.writer";
	private static final Logger LOG = LoggerFactory.getLogger(WriterConstraintValidator.class);

	@Autowired
	private WriterRepository writerRepository;

	@Override
	public void initialize(WriterConstraint constraintAnnotation) {
		LOG.info("in WriterConstraintValidator initialize()");

	}

	@Override
	public boolean isValid(List<Long> value, ConstraintValidatorContext context) {
		for (Long id : value) {
			if (!writerRepository.exists(id)) {
				return false;
			}
		}
		return true;
	}

}
