package com.bookstore.rest.constraints.validators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bookstore.domain.repositories.PublisherRepository;
import com.bookstore.rest.constraints.PublisherConstraint;

@Component
public class PublisherConstraintValidator implements ConstraintValidator<PublisherConstraint, List<Long>> {

	public static final String MESSAGE = "error.invalid.publisher";
	private static final Logger LOG = LoggerFactory.getLogger(PublisherConstraintValidator.class);

	@Autowired
	PublisherRepository publisherRepository;

	@Override
	public void initialize(PublisherConstraint constraintAnnotation) {
		LOG.info("in PublisherConstraintValidator initialize()");

	}

	@Override
	public boolean isValid(List<Long> value, ConstraintValidatorContext context) {
		for (Long id : value) {
			if (!publisherRepository.exists(id)) {
				return false;
			}
		}
		return true;
	}

}
