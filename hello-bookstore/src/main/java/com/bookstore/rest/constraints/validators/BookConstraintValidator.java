package com.bookstore.rest.constraints.validators;

import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.repositories.BookRepository;
import com.bookstore.rest.constraints.BookConstraint;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookConstraintValidator implements ConstraintValidator<BookConstraint, CreateBookDto> {

  public static final String MESSAGE = "error.invalid.createbook";

  private static final Logger LOG = LoggerFactory.getLogger(BookConstraintValidator.class);

  @Autowired
  BookRepository bookRepository;

  @Override
  public void initialize(BookConstraint constraintAnnotation) {
    LOG.info("in BookConstraintValidator initialize()");

  }

  @Override
  public boolean isValid(CreateBookDto value, ConstraintValidatorContext context) {
    List<BookEntity> booksByBarcode = bookRepository.findByBarcode(value.getBarcode());

    if (booksByBarcode.isEmpty()) {
      return true;
    } else if (isBookTitleInBooks(booksByBarcode, value.getTitle())) {
      return false;
    } else {
      LOG.warn("Books have the same barcode, but the titles are different. Therefore we will just update values.");
      return false;
    }

  }

  /**
   * @param books
   * @param title
   * @return true if there is book in books with title
   */
  boolean isBookTitleInBooks(List<BookEntity> books, String title) {
    if (books != null) {
      for (int i = 0; i < books.size(); i++) {
        if (books.get(i).getTitle().equals(title)) {
          return true;
        }
      }
    }
    return false;
  }

}
