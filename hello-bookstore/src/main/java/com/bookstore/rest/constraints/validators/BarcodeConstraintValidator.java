package com.bookstore.rest.constraints.validators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.repositories.BookRepository;
import com.bookstore.rest.constraints.BarcodeConstraint;

@Component
public class BarcodeConstraintValidator implements ConstraintValidator<BarcodeConstraint, String> {

	public static final String MESSAGE = "com.invalid.barcode";
	private static final Logger LOG = LoggerFactory.getLogger(BarcodeConstraintValidator.class);

	@Autowired
	BookRepository bookRepository;

	@Override
	public void initialize(BarcodeConstraint constraintAnnotation) {
		LOG.info("in BarcodeConstraintValidator initialize()");

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		List<BookEntity> booksByBarcode = bookRepository.findByBarcode(value);

		if (booksByBarcode.isEmpty()) {
			return true;
		}
		return false;

	}

}