package com.bookstore.rest.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.dto.detail.BookDetailDto;
import com.bookstore.domain.services.BookService;
import com.bookstore.rest.common.BookException;

import javassist.NotFoundException;

@RestController
@RequestMapping("/book")
public class BookController {

	@Autowired
	private BookService bookService;

	/**
	 * Creating book, binding used for validating.
	 *
	 * @param book
	 * @return saved book
	 * @throws NotFoundException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public BookDetailDto createBook(@RequestBody @Valid CreateBookDto book) throws BookException {

		return bookService.create(book);
	}

	/**
	 * Return book by id.
	 *
	 * @param id
	 * @return book
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	@ResponseBody
	public BookDetailDto getBookById(@PathVariable("id") Long id) {
		return bookService.getBookById(id);

	}

	/**
	 *
	 * @return list of all books
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<BookDetailDto> getBooks() {
		return bookService.getAllBooks();

	}

	/**
	 * Deleting book with id.
	 *
	 * @param id
	 * @throws NotFoundException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete")
	public void deleteBook(@RequestBody Long id) throws NotFoundException {
		bookService.delete(id);

	}

	/**
	 * Updating book with id, with data extracted from bookDTO.
	 *
	 * @param bookDetailDto
	 * @param id
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/update/{id}")
	public void updateBook(@RequestBody @Valid BookDetailDto bookDetailDto, @PathVariable Long id) {
		bookService.update(id, bookDetailDto);
	}

}
