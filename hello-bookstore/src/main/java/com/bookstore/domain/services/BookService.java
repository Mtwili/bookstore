package com.bookstore.domain.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.domain.components.BookConverter;
import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.dto.detail.BookDetailDto;
import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.entities.summaries.BookSummary;
import com.bookstore.domain.repositories.BookRepository;
import com.bookstore.rest.common.BookException;

import javassist.NotFoundException;

@Service
@Transactional
public class BookService {

	private static final Logger LOG = LoggerFactory.getLogger(BookService.class);

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private BookConverter bookConverter;

	/**
	 * Creating BookEntity from BookDetailDto and persisting it.
	 *
	 * @param bookDetailDto
	 * @return saved book converted from entity to detailDto
	 * @throws NotFoundException
	 */
	public BookDetailDto create(CreateBookDto createBookDto) throws BookException {
		if (createBookDto == null) {
			throw new BookException("CreateBookDto can not be null value.");
		} else {
			BookEntity bookEntity = bookConverter.createEntity(createBookDto);
			return createBookDTO(bookRepository.save(bookEntity));
		}
	}

	/**
	 * Receives BookEntity and if it exists updates its barcode, persists it to
	 * database and returns BookDetailDto
	 *
	 * @param bookEntity
	 * @return
	 */
	private BookDetailDto update(BookEntity bookEntity) {

		try {
			BookEntity updatedBookEntity = bookConverter.updateEntity(bookEntity);

			return createBookDTO(bookRepository.save(updatedBookEntity));
		} catch (NullPointerException ex) {
			LOG.error("No null values are allowed", ex);
			throw new NullPointerException();

		}
	}

	/**
	 *
	 * @return all books, showing only following fields Title, Writer(s),
	 *         Quantity, BookId
	 */
	public List<BookSummary> getAllBookSummary() {

		return bookRepository.findBySearchable(true);

	}

	/**
	 * Deleting Book with id from search, it still exists in database.
	 *
	 * @param id
	 * @throws NotFoundException
	 *             thrown if book with id not found
	 */
	public void delete(Long id) throws NotFoundException {

		BookEntity bookEntity = bookRepository.findOne(id);
		if (bookEntity == null) {
			LOG.warn("Book with provided id does not exist. ");
			throw new NotFoundException("Book with provided id does not exist. ");
		}
		bookEntity.setSearchable(false);
		bookRepository.save(bookEntity);
	}

	/**
	 *
	 * @param id
	 * @param bookDetailDto
	 * @return updated bookDTO after persisting to database
	 */
	public BookDetailDto update(Long id, BookDetailDto bookDetailDto) {

		try {
			BookEntity bookEntity = new BookEntity(bookDetailDto);
			bookEntity.setId(id);

			return createBookDTO(bookRepository.save(bookEntity));
		} catch (NullPointerException ex) {
			LOG.error("No null values are allowed", ex);
			throw new NullPointerException();

		}
	}

	/**
	 *
	 *
	 *
	 *
	 *
	 * @param id
	 * @param quantity
	 * @return updated version after sale of bookDTO
	 *
	 *         public BookDetailDto sellBook(Long id, Long quantity) { // TODO
	 *         finish sale BookEntity bookEntity = bookRepository.findOne(id);
	 *         Long maxQuantity = bookEntity.getQuantity();
	 *
	 *         if (maxQuantity >= quantity) { if (maxQuantity == quantity) {
	 *         bookEntity.setSearchable(false); }
	 *         bookEntity.setQuantity(maxQuantity - quantity);
	 *
	 *         } else { LOG.info("There is not enough books for sale {1} is
	 *         maximum you wanted {2}", maxQuantity, quantity); throw new
	 *         IllegalArgumentException( "There is not enough books for sale " +
	 *         maxQuantity + " is maximum but you wanted " + quantity); } return
	 *         createBookDTO(bookEntity); }
	 */
	/**
	 *
	 * @return list of all books
	 */
	public List<BookDetailDto> getAllBooks() {

		List<BookEntity> books = bookRepository.findAll();

		if (books == null) {
			LOG.warn("Can't find books.");
			return new ArrayList<>();
		} else {
			return books.stream().map(this::createBookDTO).collect(Collectors.toList());
		}

	}

	/**
	 *
	 * @param id
	 * @return book by id
	 */
	public BookDetailDto getBookById(Long id) {

		BookEntity book = bookRepository.findOne(id);

		if (book == null || !book.getSearchable()) {
			LOG.warn("Can't find book with id " + id + ".");
			return null;
		} else {
			return createBookDTO(book);
		}

	}

	/**
	 *
	 * @param title
	 * @return list of all bookDTOs with title
	 * 
	 *         public List<BookDetailDto> getBookByTitle(String title) {
	 * 
	 *         List<BookEntity> books = bookRepository.findByTitle(title);
	 * 
	 *         if (books == null) { LOG.warn("Can't find book title " + title +
	 *         "."); return new ArrayList<>(); } else { return
	 *         books.stream().map(this::createBookDTO).collect(Collectors.toList());
	 *         } }
	 **/
	/**
	 *
	 * @param publisher
	 * @return list of all bookDTOs from publisher
	 *
	 *         public List<BookDetailDto> getBookByPublisher(String publisher) {
	 *
	 *         List<BookEntity> books =
	 *         bookRepository.findByPublisher(publisher);
	 *
	 *         if (books == null) { LOG.warn("Can't find book published by " +
	 *         publisher + "."); return new ArrayList<>(); } else { return
	 *         books.stream().map(this::createBookDTO).collect(Collectors.toList());
	 *         }
	 *
	 *         }
	 * 
	 * 
	 */

	/**
	 *
	 * @param bookEntity
	 * @return converted bookEntity to bookDTO
	 */
	private BookDetailDto createBookDTO(BookEntity bookEntity) {

		if (bookEntity.getSearchable()) {
			return bookConverter.createDetail(bookEntity);
		}

		return null;
	}

}
