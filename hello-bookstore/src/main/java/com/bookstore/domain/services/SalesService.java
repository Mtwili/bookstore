package com.bookstore.domain.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.entities.SalesEntity;
import com.bookstore.domain.repositories.SalesRepository;

import javassist.NotFoundException;

@Service
public class SalesService {

	@Autowired
	private SalesRepository saleRepository;

	// TODO
	public BookEntity getBestSeller() throws NotFoundException {
		List<SalesEntity> sales = saleRepository.findAll();

		if (sales == null) {
			throw new NotFoundException("sales");
		} else {
			return sales.get(0).getBook();
		}

	}

}
