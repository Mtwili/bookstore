package com.bookstore.domain.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bookstore.domain.dto.detail.WriterDetailDto;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "writer")
public class WriterEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	private Long id;

	private String name;

	private String surname;

	@ManyToMany(targetEntity = BookEntity.class, fetch = FetchType.LAZY, mappedBy = "writers")
	@JsonManagedReference
	private List<BookEntity> books = new ArrayList<>();

	public WriterEntity() {
		super();

	}

	public WriterEntity(WriterDetailDto writerkDTO) {
		if (writerkDTO != null) {
			BeanUtils.copyProperties(writerkDTO, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<BookEntity> getBookEntities() {
		return books;
	}

	public void setBookEntities(List<BookEntity> books) {
		this.books = books;
	}

}
