package com.bookstore.domain.entities.summaries;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.bookstore.domain.dto.detail.WriterDetailDto;
import com.bookstore.domain.entities.BookEntity;

@Projection(name = "bookSummary", types = { BookEntity.class })
public interface BookSummary {

	String getTitle();

	List<WriterDetailDto> getWriters();

	Long getQuantity();

	String getBarcode();

}
