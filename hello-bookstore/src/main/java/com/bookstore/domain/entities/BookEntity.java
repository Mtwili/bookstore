package com.bookstore.domain.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.BeanUtils;

import com.bookstore.domain.dto.detail.BookDetailDto;
import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * Entity implementation class for Entity: BookEntity
 *
 */
@Entity
@Table(name = "book")
public class BookEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "barcode", unique = true, nullable = false, length = 13)
	@EAN
	private String barcode;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "writers", nullable = false)
	@ManyToMany(targetEntity = WriterEntity.class, cascade = CascadeType.ALL)
	@NotNull
	@NotEmpty
	@JsonBackReference
	private List<WriterEntity> writers;

	@Column(name = "publishers", nullable = false)
	@ManyToMany(targetEntity = PublisherEntity.class, cascade = CascadeType.ALL)
	@NotNull
	@NotEmpty
	@JsonBackReference
	private List<PublisherEntity> publishers;

	@Column(name = "number_of_pages")
	private Long numberOfPages;

	private BigDecimal price;

	private Long quantity;

	private Boolean searchable;

	public BookEntity() {
		super();
	}

	public BookEntity(BookDetailDto bookDetailDto) {
		if (bookDetailDto != null) {
			BeanUtils.copyProperties(bookDetailDto, this);
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<WriterEntity> getWriters() {
		return this.writers;
	}

	public void setWriters(List<WriterEntity> writers) {
		this.writers = writers;
	}

	public List<PublisherEntity> getPublishers() {
		return this.publishers;
	}

	public void setPublishers(List<PublisherEntity> publishers) {
		this.publishers = publishers;
	}

	public Long getNumberOfPages() {
		return this.numberOfPages;
	}

	public void setNumberOfPages(Long i) {
		this.numberOfPages = i;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getSearchable() {
		return this.searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

}
