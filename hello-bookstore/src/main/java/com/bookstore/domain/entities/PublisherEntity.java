package com.bookstore.domain.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bookstore.domain.dto.detail.PublisherDetailDto;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "publisher")
public class PublisherEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	private Long id;

	private String name;

	private String country;

	private String headquarters;

	@ManyToMany(targetEntity = BookEntity.class, fetch = FetchType.LAZY, mappedBy = "publishers")
	@JsonManagedReference
	private List<BookEntity> books = new ArrayList<>();

	public PublisherEntity() {
		super();
	}

	public PublisherEntity(PublisherDetailDto publisherDetailDto) {
		if (publisherDetailDto != null) {
			BeanUtils.copyProperties(publisherDetailDto, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}

	public List<BookEntity> getBookEntities() {
		return books;
	}

	public void setBookEntities(List<BookEntity> books) {
		this.books = books;
	}

}
