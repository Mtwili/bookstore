package com.bookstore.domain.components;

import com.bookstore.domain.dto.crud.CreateBookDto;
import com.bookstore.domain.dto.crud.UpdateBookDto;
import com.bookstore.domain.dto.detail.BookDetailDto;
import com.bookstore.domain.dto.detail.PublisherDetailDto;
import com.bookstore.domain.dto.detail.WriterDetailDto;
import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.entities.PublisherEntity;
import com.bookstore.domain.entities.WriterEntity;
import com.bookstore.domain.repositories.BookRepository;
import com.bookstore.domain.repositories.PublisherRepository;
import com.bookstore.domain.repositories.WriterRepository;
import com.bookstore.rest.common.BookException;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BookConverter {

  @Autowired
  public PublisherRepository publisherRepository;

  @Autowired
  public WriterRepository writerRepository;

  @Autowired
  public BookRepository bookRepository;

  public BookDetailDto createDetail(BookEntity bookEntity) {
    BookDetailDto bookDetailDto = new BookDetailDto();

    bookDetailDto.setId(bookEntity.getId());
    bookDetailDto.setBarcode(bookEntity.getBarcode());
    bookDetailDto.setTitle(bookEntity.getTitle());

    List<PublisherDetailDto> publishersDto = new ArrayList<>();
    List<PublisherEntity> publishers = bookEntity.getPublishers();
    for (PublisherEntity publisher : publishers) {
      PublisherDetailDto publisherDto = new PublisherDetailDto();
      BeanUtils.copyProperties(publisher, publisherDto);
      publishersDto.add(publisherDto);
    }
    bookDetailDto.setPublishers(publishersDto);

    List<WriterDetailDto> writersDto = new ArrayList<>();
    List<WriterEntity> writers = bookEntity.getWriters();
    for (WriterEntity writer : writers) {
      WriterDetailDto writerDto = new WriterDetailDto();
      BeanUtils.copyProperties(writer, writerDto);
      writersDto.add(writerDto);
    }
    bookDetailDto.setWriters(writersDto);

    bookDetailDto.setNumberOfPages(bookEntity.getNumberOfPages());
    bookDetailDto.setQuantity(bookEntity.getQuantity());
    bookDetailDto.setPrice(bookEntity.getPrice());

    return bookDetailDto;
  }

  public BookEntity createEntity(CreateBookDto dto) {
    BookEntity bookEntity = new BookEntity();

    bookEntity.setBarcode(dto.getBarcode());
    bookEntity.setTitle(dto.getTitle());

    List<PublisherEntity> publishers = new ArrayList<>();
    List<Long> publishersId = dto.getPublishers();
    for (Long id : publishersId) {
      PublisherEntity publisher = publisherRepository.findOne(id);
      publishers.add(publisher);
    }
    bookEntity.setPublishers(publishers);

    List<WriterEntity> writers = new ArrayList<>();
    List<Long> writersId = dto.getWriters();
    for (Long id : writersId) {
      WriterEntity writer = writerRepository.findOne(id);
      writers.add(writer);
    }
    bookEntity.setWriters(writers);

    bookEntity.setNumberOfPages(dto.getNumberOfPages());
    bookEntity.setQuantity(dto.getQuantity());
    bookEntity.setPrice(dto.getPrice());

    bookEntity.setSearchable(dto.getQuantity() > 0);

    return bookEntity;
  }

  public BookEntity updateEntity(UpdateBookDto dto) {
    // TODO
    return null;
  }

  public BookEntity updateEntity(BookEntity bookEntity) {
    BookEntity book = bookRepository.findOne(bookEntity.getId());
    if (book == null) {
      throw new BookException("Database mess");
    }
    bookEntity.setBarcode(book.getBarcode());

    return bookEntity;
  }

}
