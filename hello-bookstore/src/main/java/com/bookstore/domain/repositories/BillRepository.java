package com.bookstore.domain.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.domain.entities.BillEntity;

@Repository
public interface BillRepository extends CrudRepository<BillEntity, Long> {

	List<BillEntity> findAll();
}
