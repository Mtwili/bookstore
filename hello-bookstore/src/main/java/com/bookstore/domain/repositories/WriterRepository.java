package com.bookstore.domain.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.domain.entities.WriterEntity;

@Repository
public interface WriterRepository extends CrudRepository<WriterEntity, Long> {

	@Override
	WriterEntity findOne(Long id);
}
