package com.bookstore.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.domain.entities.BookEntity;
import com.bookstore.domain.entities.summaries.BookSummary;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {

	@Override
	BookEntity findOne(Long id);

	List<BookEntity> findByQuantityGreaterThan(Long available);

	List<BookEntity> findByTitle(String title);

	// List<BookEntity> findByPublisher(String publisher);

	List<BookEntity> findByBarcode(String barcode);

	List<BookSummary> findBySearchable(Boolean searchable);

}
