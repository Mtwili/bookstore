package com.bookstore.domain.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.domain.entities.PublisherEntity;

@Repository
public interface PublisherRepository extends CrudRepository<PublisherEntity, Long> {

}
