package com.bookstore.domain.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bookstore.domain.entities.SalesEntity;

@Repository
public interface SalesRepository extends CrudRepository<SalesEntity, Long> {

	@Override
	List<SalesEntity> findAll();
}
