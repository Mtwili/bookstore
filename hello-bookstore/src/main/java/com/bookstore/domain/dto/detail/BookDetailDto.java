package com.bookstore.domain.dto.detail;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.NotEmpty;

public class BookDetailDto {

	@NotNull
	private Long id;

	@NotNull
	@EAN
	private String barcode;

	@NotNull
	private String title;

	@NotNull
	@NotEmpty
	private List<WriterDetailDto> writers;

	@NotNull
	@NotEmpty
	private List<PublisherDetailDto> publishers;

	@Min(1)
	private Long numberOfPages;

	@Min(0)
	private BigDecimal price;

	@Min(0)
	private Long quantity;

	public BookDetailDto() {
		super();
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<WriterDetailDto> getWriters() {
		return this.writers;
	}

	public void setWriters(List<WriterDetailDto> writers) {
		this.writers = writers;
	}

	public List<PublisherDetailDto> getPublishers() {
		return this.publishers;
	}

	public void setPublishers(List<PublisherDetailDto> publishers) {
		this.publishers = publishers;
	}

	public Long getNumberOfPages() {
		return this.numberOfPages;
	}

	public void setNumberOfPages(Long i) {
		this.numberOfPages = i;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
