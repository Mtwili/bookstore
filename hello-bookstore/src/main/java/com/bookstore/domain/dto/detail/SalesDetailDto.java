package com.bookstore.domain.dto.detail;

import com.bookstore.domain.entities.BillEntity;
import com.bookstore.domain.entities.BookEntity;

public class SalesDetailDto {

	private BookEntity book;
	private BillEntity bill;
	private Long quantity;
	private Long id;

	public SalesDetailDto() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BookEntity getBook() {
		return book;
	}

	public void setBook(BookEntity book) {
		this.book = book;
	}

	public BillEntity getBill() {
		return bill;
	}

	public void setBill(BillEntity bill) {
		this.bill = bill;
	}

}
