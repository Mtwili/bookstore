package com.bookstore.domain.dto.detail;

import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bookstore.domain.entities.PublisherEntity;

public class PublisherDetailDto {

	@NotNull
	private Long id;

	private String name;
	private String country;
	private String headquarters;

	public PublisherDetailDto() {
		super();
	}

	public PublisherDetailDto(PublisherEntity publisherEntity) {

		if (publisherEntity != null) {
			BeanUtils.copyProperties(publisherEntity, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}
}
