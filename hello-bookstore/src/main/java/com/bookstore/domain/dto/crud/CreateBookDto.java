package com.bookstore.domain.dto.crud;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.EAN;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import com.bookstore.rest.constraints.BookConstraint;
import com.bookstore.rest.constraints.PublisherConstraint;
import com.bookstore.rest.constraints.WriterConstraint;

@Validated
@BookConstraint
public class CreateBookDto {

	@NotNull
	@EAN
	private String barcode;

	@NotNull
	private String title;

	@NotNull
	@NotEmpty
	@WriterConstraint
	private List<Long> writers;

	@NotNull
	@NotEmpty
	@PublisherConstraint
	private List<Long> publishers;

	@Min(1)
	private Long numberOfPages;

	@Min(0)
	private BigDecimal price;

	@Min(0)
	private Long quantity;

	private Boolean searchable;

	public CreateBookDto() {

		super();
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Long> getWriters() {
		return writers;
	}

	public void setWriters(List<Long> writers) {
		this.writers = writers;
	}

	public Long getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(Long numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Boolean getSearchable() {
		return searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

	public void setPublishers(List<Long> publishers) {
		this.publishers = publishers;
	}

	public List<Long> getPublishers() {
		return publishers;
	}

}
