package com.bookstore.domain.dto.detail;

import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import com.bookstore.domain.entities.WriterEntity;

public class WriterDetailDto {

	@NotNull
	private Long id;

	private String name;
	private String surname;

	public WriterDetailDto() {
		super();
	}

	public WriterDetailDto(WriterEntity writeEntity) {

		if (writeEntity != null) {
			BeanUtils.copyProperties(writeEntity, this);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
}
