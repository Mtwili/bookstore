package com.bookstore.domain.dto.detail;

import java.math.BigDecimal;
import java.util.Date;

public class BillDetailDto {
	private Date transactionTime;
	private BigDecimal price;
	private Long id;

	public BillDetailDto() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}

}
